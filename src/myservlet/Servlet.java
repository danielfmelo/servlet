package myservlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        float numa = 0;
        float numb = 0;
        String result = "";

        try{
            numa = Float.parseFloat(request.getParameter("numa"));
            numb = Float.parseFloat(request.getParameter("numb"));

            switch (request.getParameter("send")){
                case "Sum":
                    result = "<p>Result: " + (numa + numb) + "</p>";
                    break;
                case "Subtract":
                    result = "<p>Result: " + (numa - numb) + "</p>";
                    break;
                case "Multiply":
                    result = "<p>Result: " + (numa * numb) + "</p>";
                    break;
                case "Divide":
                    result = "<p>Result: " + (numa / numb) + "</p>";
                    break;
                default:
                    result = "<p>Result: " + (numa + numb) + "</p>";
                    break;
            }

        }catch (NumberFormatException exception) {
            result = "<p>Result: Invalid character </p>";
        }

        out.println("<h1>Calculator</h1>");
        out.println(result);
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
